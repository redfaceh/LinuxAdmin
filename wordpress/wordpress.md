## 1. 사전준비
- php 7.3 이상
- MariaDB 10.1 이상 또는 MySQL 5.6
- Apache 또는 Nginx

### 1) Apache 설치
```
web# yum -y install httpd
```
```
web# systemctl start httpd
```
```
web# systemctl enable httpd
```
```
web# firewall-cmd --add-service=http
```
```
web# firewall-cmd --add-service=http --permanent
```

### 2) php 설치
> 참고: https://rpms.remirepo.net/

```
web# yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
```

```
web# yum install -y https://rpms.remirepo.net/enterprise/remi-release-7.rpm
```
```
web# yum install -y yum-utils
```

```
web# yum-config-manager --enable remi-php74
```

```
web# yum install -y php php-mysql
```

```
web# systemctl restart httpd
```

### 3) MariaDB 준비
> 참고: https://mariadb.org/download/

```
db# vi /etc/yum.repos.d/mariadb.repo
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.4/centos7-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
```

```
db# yum install -y MariaDB-server
```

```
db# systemctl start mariadb
```

```
db# systemctl enable mariadb
```

```
db# firewall-cmd --add-service=mysql
```
```
db# firewall-cmd --add-service=mysql --permanent
```

```
db# mysql_secure_installation
```



## 2. Wordpress 설치

### 1) wordpress 데이터베이스 준비
```
db# mysql -u root -p
```

```
MariaDB [(none)]> CREATE DATABASE wordpress;
```

```
MariaDB [(none)]> GRANT ALL PRIVILEGES ON wordpress.* TO wordpress@'10.0.0.%' IDENTIFIED BY 'dkagh1.';
```

```
MariaDB [(none)]> FLUSH PRIVILEGES; 
```

### 2) 설치 파일 다운로드

```
web# curl -o latest.tar.gz https://wordpress.org/latest.tar.gz
```

### 3) 파일 압축 해제
```
web# tar xzf latest.tar.gz
```

### 4) wordpress 파일 /var/www/html 로 복사
```
web# cp -r wordpress/* /var/www/html/
```

### 5) /var/www/html에 apache 사용자 쓰기 권한 추가
```
web# setfacl -Rm u:apache:rwX /var/www/html 
```

### 6) 웹브라우저에서 접속
> http://web서버주소/wp-admin/install.php