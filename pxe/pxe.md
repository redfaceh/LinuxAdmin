## 0. SELinux 비활성화
```bash
setenfoce 0
sed -i 's/SELINUX=enforcing/SELINUX=permissive/' /etc/selinux/config
```

## 1. dhcp 구성
### 1) 패키지설치
```bash
yum -y install dhcp
```

### 2) 설정파일 생성
```bash
cat << EOF > /etc/dhcp/dhcpd.conf
allow booting;
allow bootp;
default-lease-time 600;
max-lease-time 7200;
option domain-name-servers 8.8.8.8;
option ip-forwarding false;
option mask-supplier false;
ddns-update-style interim;
next-server 192.168.122.10;
filename "pxelinux.0";

subnet 192.168.122.0 netmask 255.255.255.0 {   
        option routers 192.168.122.2;
        range 192.168.122.101 192.168.122.199; 
}
EOF
```

### 3) 서비스 실행
```bash
systemctl start dhcpd 
systemctl enable dhcpd 
```

### 4) 서비스 오픈
```bash
firewall-cmd --add-service=dhcp
firewall-cmd --add-service=dhcp --permanent 
```

## 2. tftp 구성
### 1) 패키지 설치
```bash
yum -y install tftp tftp-server xinetd
```

### 2) 설정 파일 수정
```bash
vi /etc/xinetd.d/tftp
14         disable                 = no 
```

### 3) 서비스 실행
```bash
systemctl start xinetd
```

### 4) 포트 오픈
```bash
firewall-cmd --add-port=69/tcp --add-port=69/udp --add-port=4011/udp
firewall-cmd --add-port=69/tcp --add-port=69/udp --add-port=4011/udp --permanent
```

## 3. PXE 구성 복사
### 1) syslinux 패키지 설치
```bash
yum -y install syslinux 
```

### 2) 부팅 관련 파일 복사
```bash
cp /usr/share/syslinux/{chain.c32,mboot.c32,menu.c32,pxelinux.0} /var/lib/tftpboot/
```

### 3) 부팅 파일 생성
```bash
mkdir -p /var/lib/tftpboot/pxelinux.cfg 
cat << EOF >  /var/lib/tftpboot/pxelinux.cfg/default
default menu.c32
prompt 0timeout 30
MENU TITLE redfaceh.info PXE Menu
LABEL centos7_x64
MENU LABEL CentOS 7_x64
KERNEL /networkboot/centos/vmlinuz
APPEND initrd=/networkboot/centos/initrd.img inst.repo=ftp://192.168.122.10/pub/centos  ks=ftp://192.168.122.10/pub/centos/centos7.cfg

EOF
```

## 4. FTP 구성
### 0) 이미지 다운로드 및 마운트
```bash
curl -o centos7.iso http://mirror.kakao.com/centos/7.8.2003/isos/x86_64/CentOS-7-x86_64-Minimal-2003.iso
mount centos7.iso /mnt
```

### 1) 패키지 설치
```bash
yum -y install vsftpd  
```

### 2) 이미지 파일 FTP 서비스로 복사
```bash
mkdir /var/ftp/pub/centos
cp -r /mnt/* /var/ftp/pub/centos/
```

### 3) vmlinuz와 initrd 파일 TFTP로 복사
```bash
mkdir -p /var/lib/tftpboot/networkboot/centos
cp /var/ftp/pub/centos/images/pxeboot/{initrd.img,vmlinuz} /var/lib/tftpboot/networkboot/centos/
```

### 4) 서비스 실행
```bash
systemctl start vsftpd.service
systemctl enable vsftpd.service
```

### 5) 서비스 오픈
```bash
firewall-cmd --add-service=ftp
firewall-cmd --add-service=ftp --permanent
```

### 6) Kickstart 파일 다운로드
```bash
curl -o /var/ftp/pub/centos/centos7.cfg http://redface7.iptime.org:9000/FILEs/ks.cfg
```