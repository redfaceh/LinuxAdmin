## 1. 서버 준비
- proxy
  - eth0: 192.168.122.10/24 (nat)
  - eth1: 192.168.123.10/24 (internal)
- web1
  - eth0: 192.168.123.11/24 (internal)
- web2
  - eth0: 192.168.123.12/24 (internal)


## 2. 방화벽 규칙 설정

### 1) 마스커레이드 활성화
```
proxy# firewall-cmd --chagne-interface=eth1 --zone=external
```

### 2) NAT 인터페이스로 eth0 지정
```
proxy# firewall-cmd --direct --add-rule ipv4 nat POSTROUTING 0 -o eth0 -j MASQUERADE
```

### 3) In: eth1 -> Out: eht0
```
proxy# firewall-cmd --direct --add-rule ipv4 filter FORWARD 0 -i eth1 -o eth0 -j ACCEPT
```

### 4) 응답 패킷의 허용
```
proxy# firewall-cmd --direct --add-rule ipv4 filter FORWARD 0 -i eth0 -o eth1 -m state --state RELATED,ESTABLISHED -j ACCEPT
```

## 3. 웹 서버 설정
```
web1# nmcli connection modify eth0 ipv4.gateway 192.168.123.10/24
```

```
web2# nmcli connection modify eth0 ipv4.gateway 192.168.123.10/24
```

