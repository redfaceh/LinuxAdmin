## 1. 서버 준비
- proxy
  - eth0: 192.168.122.10/24 (nat)
- web1
  - eth0: 192.168.122.11/24 (nat)
- web2
  - eth0: 192.168.122.12/24 (nat)

## 2. haproxy 설정
### 1) 패키지 설치
```
proxy# yum -y install haproxy
```

### 2) 설정 파일 편집
```
proxy# vi /etc/haproxy/haproxy.cfg
...
frontend web
    bind *:80
    default_backend   servers
    option forwardfor

backend servers
    balance roundrobin
    server web1 192.168.122.11:80 check 
    server web2 192.168.122.12:80 check 
```

### 3) 서비스 실행
```
proxy# systemctl start haproxy
```
```
proxy# systemctl enable haproxy
```


## 3. web 서버 준비
- httpd 패키지 설치 및 서비스와 방화벽 오픈... 늘했던 거
- web1 서버의 /var/www/html/index.html에는 web1 내용 삽입
- web2 서버의 /var/www/html/index.html에는 web2 내용 삽입